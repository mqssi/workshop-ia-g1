# Reconnaissance d'aliment via système d'apprenstissage avancé (TensorFlow)

## Installation

Afin d'utiliser le projet, il vous faut python d'installé. 
Installez les dépendenses :

```python
pip install -r requirements.txt
```
## Utilisation concrète

L'IA a été entrainée pour avoir comme fond un écran noir. Pour ajouter les aliments présents dans la liste (liste.txt), il suffit de les exposer à la caméra (de préférences proche de la caméra).

Lancer le script :

```python
python3 openCamerOG.py 
```
Pour arreter le script, mettre la fenêtre de la webcam en avant et appuyer sur "q"
Un récapitulatif sera alors généré.

##
