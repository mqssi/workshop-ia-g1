from numpy import mgrid
from sklearn.neural_network import MLPClassifier
import joblib

# Données exemples
# Fer , MG, SD, Ps

X = [[10.49, 308.2, 254, 2660.2], 
[8.42, 209.4, 3800, 2502.0],
[6.3, 260.3, 158.0, 2107.2], 
[7.4, 350.4, 8000.2, 2007.3],
[7.2, 320, 4002, 1500.2], 
[8.8, 401.2, 3708.2, 2940.2],
[2.4, 387.2, 2900.2, 2949.2], 
[9.95, 85.6, 148.20, 3050.9]]

# Réponses attendues 
# 0 saine
# 1 Carrence Fer
# 2 Exces Sd
# 3 Carrence PS
# 4 Carrence Mg
# 5 alimentation déséquilibrée
y = [5, 
4,
5,
2,
3,
0,
1,
5]
  
# Création d'un MultiLayerPercetron (MLP)
# Changer éventuellement Hidden Layer Sizes
clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(50), random_state=1)

# Entrainer le Réseau
clf.fit(X, y)

# Sauvegarder le Réseau dans un fichier
joblib.dump(clf, 'monModel.pkl')

# Effectuer une prediction
pred = clf.predict([[1.4, 1., 0.01, 0.14]])
# Afficher la prédiction
print(pred)

"""
A utiliser pour recharger le model pré-entrainé
"""
clf_fromFile = joblib.load('monModel.pkl')
# Effectuer une prediction
pred = clf_fromFile.predict([[1.4, 1., 0.01, 0.14]])
print(pred)
