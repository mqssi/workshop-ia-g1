# import the opencv library
from glob import glob
import cv2
from keras.models import load_model
from PIL import Image, ImageOps
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import time
from playsound import playsound



# Load the model
model1 = load_model('keras_model.h5')
#model2 = load_model('keras_model_1.h5')

# Create the array of the right shape to feed into the keras model
# The 'length' or number of images you can put into the array is
# determined by the first position in the shape tuple, in this case 1.
data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)
  
# define a video capture object
vid = cv2.VideoCapture(0)
#vid.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
#vid.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
labels = ['Viande',
'Pain',
'Fond',
'Haricot',
'Brocoli',
'Rice',
'Banane',
'Avocat',
'Arachide',
'Sucrerie'
]


labelCount = 0
previousLabel = ""

nbViande = 0
nbBanane = 0
nbHaricot = 0
nbPain = 0
nbArachide = 0
nbBroco = 0
nbAvocat = 0
nbArachide = 0
nbRice = 0
nbSucrerie = 0

tauxFerViande = 2.2
tauxFerPain = 6.8
tauxFerBanane = 0.2
tauxFerBrocoli = 0.76
tauxFerHaricot = 1.08
tauxFerRice = 0.6
tauxFerArachide = 0.0172
tauxFerAvocat = 0.34

tauxMgViande = 24.4
tauxMgPain = 108
tauxMgBanane = 28
tauxMgBrocoli = 21.9
tauxMgHaricot = 56  
tauxMgRice = 35
tauxMgArachide = 70.6
tauxMgAvocat = 21

tauxSdViande = 57
tauxSdPain = 2.9
tauxSdBanane = 5
tauxSdBrocoli = 19
tauxSdHaricot = 6  
tauxSdRice = 5
tauxSdArachide = 5
tauxSdAvocat = 6

tauxPotassiumViande = 319
tauxPotassiumHaricot = 370
tauxPotassiumAvocat = 430
tauxPotassiumPain = 291
tauxPotassiumBrocolis = 357 
tauxPotassiumRice = 98
tauxPotassiumArachide = 54.2
tauxPotassiumBanane = 320

def responseM1(labelName):

    global previousLabel 
    global nbViande
    global nbBanane
    global nbHaricot
    global nbPain
    global nbArachide
    global nbBroco
    global nbAvocat
    global nbArachide
    global nbRice
    global nbSucrerie
    
    print("Label : " + labelName)
    print("")
    
    if (previousLabel == "") :
        previousLabel = labelName   
    elif (previousLabel != labelName):
        print("Nouvel objet")
        if (labelName == "Viande"):
            nbViande += 1
            print("Nb de Viande",nbViande)
            print("----------------------")
            playsound("./Good.mp3")
            
        if (labelName == "Pain"):
            nbPain += 1
            print("Nb de Pain",nbPain)
            print("----------------------")
            playsound("./Good.mp3")
        
        if (labelName == "Haricot"):
            nbHaricot += 1
            print("Nb de Haricot",nbHaricot)
            print("----------------------")
            playsound("./Good.mp3")
        
        if (labelName == "Brocoli"):
            nbBroco += 1
            print("Nb de brocolis",nbBroco)
            print("----------------------")  
            playsound("./Good.mp3")
            
        if (labelName == "Rice"):
            nbRice += 1
            print("Nb de riz",nbRice)
            print("----------------------")
            playsound("./Good.mp3")  
            
        if (labelName == "Banane"):
            nbBanane += 1
            print("Nb de bananes",nbBanane)
            print("----------------------") 
            playsound("./Good.mp3")
            
        if (labelName == "Avocat"):
            nbAvocat += 1
            print("Nb d'avocats",nbAvocat)
            print("----------------------")
            playsound("./Good.mp3") 
            
        if (labelName == "Arachide"):
            nbArachide += 1
            print("Nb d'arachides",nbArachide)
            print("----------------------")
            playsound("./Good.mp3")
        
        if (labelName == "Sucrerie"):
            nbSucrerie += 1
            print("Nb de sucreries", nbSucrerie)
            print("----------------------")
            playsound("./NotGood.mp3")        
              
        previousLabel = labelName


def dataFetcher():

    tauxFerUser = ((tauxFerViande*nbViande) + ((tauxFerPain*nbPain)) +(tauxFerAvocat*nbAvocat ) + (tauxFerBanane* nbBanane) +  (tauxFerHaricot* nbHaricot) + (tauxFerArachide* nbArachide) + (tauxFerRice* nbRice) +( tauxFerBrocoli*nbBroco))
    
    tauxMgUser = ((tauxMgViande*nbViande) + ((tauxMgPain*nbPain)) +(tauxMgAvocat*nbAvocat ) + (tauxMgBanane* nbBanane) +  (tauxMgHaricot* nbHaricot) + (tauxMgArachide* nbArachide) + (tauxMgRice* nbRice) +( tauxMgBrocoli*nbBroco))

    tauxSdUder = ((tauxSdViande*nbViande) + ((tauxSdPain*nbPain)) +(tauxSdAvocat*nbAvocat ) + (tauxSdBanane* nbBanane) +  (tauxSdHaricot* nbHaricot) + (tauxSdArachide* nbArachide) + (tauxSdRice* nbRice) +( tauxSdBrocoli*nbBroco))
    
    tauxPotassiumUser = ((tauxPotassiumViande * nbViande) + ((tauxPotassiumPain * nbPain)) +(tauxPotassiumAvocat * nbAvocat ) + (tauxPotassiumBanane * nbBanane) +  (tauxPotassiumHaricot * nbHaricot) + (tauxPotassiumArachide * nbArachide) + (tauxPotassiumRice * nbRice) +( tauxPotassiumBrocolis * nbBroco))
    
    
    Dict = {'Viandes': nbViande, 'Bananes':nbBanane, 'Haricot' : nbHaricot, 'Brocolis': nbBroco, 'Avocats': nbAvocat, 'Pain': nbPain, 'Riz' : nbRice, 'Arachides' : nbArachide }
    
    DictMacro = {'Taux de fer (mg) : ': tauxFerUser, 'Taux de Magnesium (mg) : ' : tauxMgUser, 'Taux de Sodium (mg) : ': tauxSdUder,'Taux de Potassium (mg) : ': tauxPotassiumUser}
    
    print("")
    print("++++++++++++++++")
    print(" Récapitulatif des aliments détectés : ")
    print(Dict)
    print("++++++++++++++++")
    print("")
    print(">>>>>>>>>>>>>>>>")
    print(" Récapitulatif des macronutriments détectés : " )
    print(DictMacro)
    print(">>>>>>>>>>>>>>>>")
    
    
    N = 4
    tauxRecu = (tauxMgUser, tauxPotassiumUser, tauxSdUder, tauxFerUser)
    tauxConseil = (420, 3100, 15, 9)
    menStd = (2, 3, 4, 1)
    womenStd = (2, 3, 4, 1)
    ind = np.arange(N)    # the x locations for the groups
    width = 0.60       # the width of the bars: can also be len(x) sequence
    
    fig, ax = plt.subplots()

    p1 = ax.bar(ind, tauxRecu, width, yerr=menStd, label='Dectecté')
    p2 = ax.bar(ind, tauxConseil, width,
                 bottom=tauxRecu, yerr=womenStd, label='Recommandé')

    ax.axhline(0, color='grey', linewidth=0.9)
    ax.set_ylabel('Unité en mg')
    ax.set_title('Bilan des macronutriments')
    ax.set_xticks(ind, labels=['Magnésium', 'Potassium', 'Fer', 'Sodium'])
    ax.legend()

    # Label with label_type 'center' instead of the default 'edge'
    ax.bar_label(p1, label_type='center')
    ax.bar_label(p2, label_type='center')
    #ax.bar_label(p2)

    plt.show()
    
  
counter = 0
thresold = 100
while(True):
      
    # Capture the video frame
    # by frame
    ret, frame = vid.read()
    frame = cv2.flip(frame,1)
    
    if counter >= thresold:
        # Replace this with the path to your image
        image = frame
        #resize the image to a 224x224 with the same strategy as in TM2:
        #resizing the image to be at least 224x224 and then cropping from the center
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_PIL = Image.fromarray(image)
        size = (224, 224)
        image = ImageOps.fit(image_PIL, size, Image.ANTIALIAS)
        image_array = np.asarray(image)
        # Normalize the image
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
        # Load the image into the array
        data[0] = normalized_image_array

        # run the inference
        prediction = model1.predict(data,verbose=0)
        #prediction2 = model2.predict(data,verbose=0)
        
        predIdx = np.argmax(prediction)
        responseM1(labels[predIdx])
        
        
        #predIdx = np.argmax(prediction2)
        #responseM2(labels1[predIdx])
        
        counter = 0
        cv2.imwrite('test_pred'+str(counter)+'.jpg',image_array)

        
    counter += 1
    
    # Display the resulting frame
    cv2.imshow('frame', frame)
      
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        dataFetcher()
        break
  
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()